import React from 'react';
import './App.css';
import { Route } from 'react-router-dom'
import { IonApp, IonRouterOutlet } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/home'
import Profil from './pages/profil'
import Discuss from './pages/discuss';
import Registration from './pages/registration';
import NotificationsManagement from './pages/notifications-management';
import 'font-awesome/css/font-awesome.min.css';

import Keycloak from 'keycloak-js';

import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = { 
      keycloak: null
    };
  }

  componentDidMount() {
    const keycloak = Keycloak('../keycloak.json');
    keycloak.init({onLoad: 'login-required'}).then(authenticated => {
      this.setState({ keycloak: keycloak })
    })
  }
  
  render() {
    if (this.state.keycloak) {
      console.log(this.state.keycloak)
      return (
            <IonApp>
              <IonReactRouter>
                <IonRouterOutlet>
                  <Route exact path="/" render={props => <Home {...props} keycloak={this.state.keycloak} />} />
                  <Route path="/registration" render={props => <Registration {...props} keycloak={this.state.keycloak} />} />
                  <Route path="/home" render={props => <Home {...props} keycloak={this.state.keycloak} />} />
                  <Route path="/discuss" render={props => <Discuss {...props} keycloak={this.state.keycloak} />} />
                  <Route path="/notifications" render={props => <NotificationsManagement {...props} keycloak={this.state.keycloak} />} />
                  <Route path="/profils/:idUser" render={props => <Profil {...props} keycloak={this.state.keycloak} />} />
                </IonRouterOutlet>
            </IonReactRouter>
          </IonApp>
      );
    } else {
        return(<IonApp></IonApp>)
    }
  }
}

export default App;
