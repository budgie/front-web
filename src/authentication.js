import Keycloak from 'keycloak-js';

export default function authenticate () {
    return new Promise(function (resolve, reject) {
        const keycloak = Keycloak('./keycloak.json');
        keycloak.init({onLoad: 'login-required'})
            .then(authenticated => {resolve({ keycloak: keycloak, authenticated: authenticated })})
            .catch(err => {reject(err)})
    })
}