import React from 'react';
import './profil.css'
import Publication from '../publication/publication'

class Profil extends React.Component {    
    constructor(props) {
        super(props);
        this.state = { 
            idUser: this.props.idUser,
            user: null,
            publication: [],
            idRelation: null,
            friends: null,
            editing: false,
            tempFirst_name: '',
            tempLast_name: '',
            tempEmail: '',
            reactions: [],
        };
        
        this.subscribe = this.subscribe.bind(this)
        this.unsubscribe = this.unsubscribe.bind(this)
        this.updateProfil = this.updateProfil.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.getReaction = this.getReaction.bind(this)
    }

    authorizationHeader() {
        if(!this.props.keycloak) return {};
        return {
          headers: {
            "Authorization": "Bearer " + this.props.keycloak.token
          }
        };
    }
    
    getReaction() {    
        console.log(process.env.REACT_APP_REACTION_URL+`/type`)
        fetch(process.env.REACT_APP_REACTION_URL+`/type`)
            .then(res => res.json())
            .then(res => {console.log('test');this.setState({ reactions: res.reactions })})
            .catch(err => console.log(err));
    }    

    handleChange(event) {
        console.log(this.inputfile)
        this.setState({[event.target.id]: event.target.value});
    }

    updateProfil() {

        fetch(process.env.REACT_APP_USER_URL+`/users/${this.props.keycloak.idTokenParsed.sub}`,{
            method: 'PUT',
            headers: {
              'Accept': 'application/json',
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                idUser: this.props.keycloak.idTokenParsed.sub,
                first_name: this.state.tempFirst_name,
                last_name: this.state.tempLast_name,
                email: this.state.tempEmail,
            })
        })
        .then(res => res.json())
        .then(res => {this.setState({ editing: !this.state.editing }); this.setState({user: res})})
            .catch(err => console.log(err));
    }

    subscribe() {
        fetch(process.env.REACT_APP_USER_URL+`/friends`, {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    idUser:this.props.keycloak.idTokenParsed.sub,
                    idFriend: this.props.idUser
                })
            })
                .then(res => res.json())
                .then(res => this.setState({idRelation: res.idRelation}))
                .catch(err => console.error(err));
    }

    unsubscribe() {
        fetch(process.env.REACT_APP_USER_URL+`/friends`, {
                method: 'DELETE',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    idRelation:this.state.idRelation
                })
            })
                .then(res => res.json())
                .then(res => {console.log(res);this.setState({idRelation: null})})
                .catch(err => console.error(err));
    }

    getAllFriends() {    
        fetch(process.env.REACT_APP_USER_URL+`/friends/${this.props.idUser}`)
            .then(res => res.json())
            .then(res => this.setState({ friends: res }))
            .catch(err => err);
    }

    componentDidMount() {
        
        this.getReaction()

        // recup info user
        fetch(process.env.REACT_APP_USER_URL+`/users/${this.state.idUser}`)
                .then(res => res.json())
                .then(res => this.setState({ user: res, tempFirst_name: res.first_name, tempLast_name: res.last_name, tempEmail: res.email }))
                .catch(err => console.log(err));
        
        // recup relation user connecté et user profil
        fetch(process.env.REACT_APP_USER_URL+`/friends/${this.props.keycloak.idTokenParsed.sub}/users/${this.props.idUser}`)
                .then(res => res.json())
                .then(res => this.setState({ idRelation: res }))
                .catch(err => console.log(err));
        
        // recup post user
        fetch(process.env.REACT_APP_PUBLICATION_URL+`/users/${this.state.idUser}`, this.authorizationHeader())
                .then(res => res.json())
                .then(res => {console.log(res);this.setState({ publication: res })})
                .catch(err => console.log(err));

        this.getAllFriends();
    }

    render() {
        if (this.state.user) {
            let subButton      
            let editButton       
            let first_name     
            let last_name     
            let email
            
            if (this.state.user.id === this.props.keycloak.idTokenParsed.sub) {
                subButton = null
                editButton = <div className="publication-header-edit" onClick={() => this.setState({editing: !this.state.editing})}><i className="fa fa-edit"></i></div>
            } else {
                editButton = <div></div>
                if (this.state.idRelation) {
                    subButton = <div id="subButton" className="unfollow" onClick={this.unsubscribe}>Se désabonner</div>
                }
                else {
                    subButton = <div id="subButton" className="follow" onClick={this.subscribe}>S'abonner</div>
                }
            }
            
            if (this.state.editing) {
                first_name = <input type="text" id="tempFirst_name" onChange={this.handleChange} value={this.state.tempFirst_name} />
                last_name = <input type="text" id="tempLast_name" onChange={this.handleChange} value={this.state.tempLast_name} />
                email = <input type="text" id="tempEmail" onChange={this.handleChange} value={this.state.tempEmail} />

                editButton = <div style={{display: 'inline-flex'}}>
                    <div className="publication-header-edit" onClick={this.updateProfil}>
                        <i className="fa fa-check-square"></i> Valider
                    </div>
                    <div className="publication-header-edit" onClick={() => {this.setState({editing: !this.state.editing})}}>
                        <i className="fa fa-times-circle" style={{color: 'red'}}></i> Annuler
                    </div>
                </div>
            } else {                
                first_name = this.state.user.first_name
                last_name = this.state.user.last_name
                email = this.state.user.email
            }

            if (this.state.reactions.length === 0) {       
                return (
                    <div className="publications-body">
                        <div className="no-budges">Loading <span role="img" aria-label="noBudges">🤷</span></div>
                    </div>
                );
            } else {
                console.log(this.state.reactions)
                return (
                    <div className="profils-body">
                        <div className="profils-presentation">
                        <ion-card>
                        <div className="profil-color-header"><a href="/home"><i className="fa fa-arrow-left"></i></a></div>
                            <div className="presentation-header">
                                <div className="pseudo">
                                    {this.state.user.first_name} {this.state.user.last_name}
                                </div>
                                <div> {editButton} </div>
                                <div> {subButton} </div>
                            </div>
                            <ion-list className="profils-presentation-info">
                                <ion-item className="info">Prénom: {first_name}</ion-item>
                                <ion-item className="info">Nom: {last_name}</ion-item>
                                <ion-item className="info">Email: {email}</ion-item>
                                <div className="stats">
                                    <ion-grid>
                                        <ion-row>
                                            <ion-col>Budges: {this.state.publication.length}</ion-col>     
                                        { this.state.friends && <ion-col>Abonnements: {this.state.friends.length} </ion-col> }
                                        </ion-row>
                                    </ion-grid>
                                    </div>
                            </ion-list>
                        </ion-card>
                        </div>
                        <div className="profils-posts">
                            {this.state.publication.map((item, index) => (
                                <Publication item={item} key={index} keycloak={this.props.keycloak} reactions={this.state.reactions} />
                            ))} 
                        </div>
                    </div>
                );
            }
        } else {
            return (
                <div className="profils-body">Utilisateur inconnu</div>
            );
        }
    }
  }

export default Profil;
