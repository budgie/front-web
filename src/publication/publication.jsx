import React from 'react';
import Reaction from '../reactions/reaction';
import './publication.css';

class Publication extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			reactions: this.props.reactions,
			medias: [],
			editing: false,
			tempMessage: this.props.item.message,
			message: this.props.item.message,
			reloadReaction: true
		};

		this.getMedia = this.getMedia.bind(this);
		this.deletePost = this.deletePost.bind(this);
		this.updatePost = this.updatePost.bind(this);
		this.displayDate = this.displayDate.bind(this);
		this.handleMessageChange = this.handleMessageChange.bind(this);
		this.reloadReaction = this.reloadReaction.bind(this);
	}

	reloadReaction() {
		this.setState({ reloadReaction: false });
		this.setState({ reloadReaction: true });
		console.log('test');
	}

	handleMessageChange(event) {
		this.setState({ tempMessage: event.target.value });
	}

	deletePost() {
		fetch(process.env.REACT_APP_PUBLICATION_URL + `/posts/${this.props.item.id}`, {
			method: 'DELETE',
			headers: {
				Authorization: 'Bearer ' + this.props.keycloak.token
			}
		})
			.then((res) => res.json())
			.then((res) => this.props.onDelete(this.props.item.id))
			.catch((err) => console.log(err));
	}

	getMedia() {
		fetch(process.env.REACT_APP_MEDIA_URL + `/files/posts/${this.props.item.id}`)
			.then((res) => res.json())
			.then((res) => {
				console.log(res);
				if (res.media) {
					this.setState({ medias: res.media });
				}
			})
			.catch((err) => console.log(err));
	}

	updatePost() {
		console.log('test');
		fetch(process.env.REACT_APP_PUBLICATION_URL + `/posts/${this.props.item.id}`, {
			method: 'PUT',
			headers: {
				Accept: 'application/json',
				'Content-Type': 'application/json',
				Authorization: 'Bearer ' + this.props.keycloak.token
			},
			body: JSON.stringify({
				id: this.props.item.id,
				message: this.state.tempMessage
			})
		})
			.then((res) => res.json())
			.then((res) => this.setState({ message: res.message, tempMessage: res.message }))
			.catch((err) => console.log(err));
	}

	componentDidMount() {
		this.getMedia();
	}

	displayDate(date) {
		date = new Date(date);
		return (
			(date.getDate() < 10 ? '0' + date.getDate() : date.getDate()) +
			'/' +
			(date.getMonth() < 10 ? '0' + date.getMonth() + 1 : date.getMonth() + 1)
		);
	}

	render() {
		let deleteButton;
		let editButton;
		let valueDisplay;
		let reactions;
        if (this.state.editing) {
            valueDisplay = <ion-item className="publication-body">
                <textarea 
                    ref={(input) => input && input.focus() } 
                    id='tempMessage' 
                    onChange={this.handleMessageChange} 
                    onBlur={(e) => {
                        this.setState({editing: !this.state.editing, tempMessage: this.state.message})
                    }} 
                    value={this.state.tempMessage}>
                </textarea>
                <div className="publication-header-edit" onMouseDown={this.updatePost}>
                    <i className="fa fa-check-square"></i> Valider
                </div>
            </ion-item>
        } else {                
            valueDisplay = <ion-list className="publication-body">
                <ion-item color="light">
                    <p style={{width: '100%'}}>{this.state.message}</p>
                </ion-item>
                {this.state.medias.map((media, i) => (                       
                    <ion-item>
                        <img className="publication-img-item" style={{height:"100px", width:'auto'}} src={media.path} key={i}/>
                    </ion-item>
                ))} 
            </ion-list>
        }
        

		if (this.props.item.user.id === this.props.keycloak.idTokenParsed.sub) {
			deleteButton = (
				<div className="publication-header-close" onClick={this.deletePost}>
					<i className="fa fa-times" />
				</div>
			);
			editButton = (
				<div
					className="publication-header-edit"
					onClick={() => this.setState({ editing: !this.state.editing })}
				>
					<i className="fa fa-edit" />
				</div>
			);
		} else {
			deleteButton = <div />;
			editButton = <div />;
        }
        
		if (this.state.reloadReaction) {
			reactions = (
				<div className="reactions">
					{this.state.reactions.map((reaction) => (
						<Reaction
							idPost={this.props.item.id}
							idTypeReaction={reaction.idtypereaction}
							nom={reaction.nom}
							valeur={reaction.valeur}
							key={reaction.valeur}
							reloadFunction={this.reloadReaction}
						/>
					))}
				</div>
			);

			//this.setState({reloadReaction: !this.state.reloadReaction})
		}

		return (
			<div className="publication" clef={this.props.item.id}>
				<ion-card className="publication">
					<div className="publication-header">
						<div className="publication-header-name">
							<b>
								<a href={`/profils/${this.props.item.user.id}`}>
									{this.props.item.user.first_name + ' ' + this.props.item.user.last_name}
								</a>
							</b>
							<p style={{ marginLeft: '30px' }}>{this.displayDate(this.props.item.creation_date)}</p>
						</div>
						<div className="publication-header-buttons">
							{editButton}
							{deleteButton}
						</div>
					</div>
					<ion-item className="publication-body">{valueDisplay}</ion-item>
					<div className="publication-footer">{reactions}</div>
				</ion-card>
			</div>
		);
	}
}

export default Publication;
