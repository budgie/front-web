import React from 'react';
import Publication from '../publication/publication'

import './publications.css';

class Publications extends React.Component {
        constructor(props) {
            super(props)
            this.state = {
                allPosts: null,
                publication_new_input: '',
                userInfo: null,
                relations: null,
                friendsPosts: [],
                reactions: [],
                noAction: false,
            }
            
            this.publish = this.publish.bind(this)
            this.handleChange = this.handleChange.bind(this)
            this.getReaction = this.getReaction.bind(this)
            this.deletePost = this.deletePost.bind(this)

            this.inputfile = React.createRef()
        }

        authorizationHeader() {
            if(!this.props.keycloak) return {};
            return {
              headers: {
                "Authorization": "Bearer " + this.props.keycloak.token
              }
            };
        }

        getReaction() {    
            this.setState({noAction: true})
            
            fetch(process.env.REACT_APP_REACTION_URL+`/type`)
                .then(res => res.json())
                .then(res => {console.log('test');this.setState({ reactions: res.reactions , noAction: false})})
                .catch(err => this.setState({noAction: false}));
        }

        deletePost(idPost) {
            this.setState({noAction: true})
            
            let friendsPosts = this.state.friendsPosts
            this.state.friendsPosts.forEach((post, index) => {
                if (post.id === idPost) {
                    friendsPosts.splice(index, 1)
                    return
                }
            })

            this.setState({friendsPosts: friendsPosts, noAction: false})
        }

        getAllPosts() {
            this.setState({noAction: true})

            fetch(process.env.REACT_APP_PUBLICATION_URL+"/posts")
                .then(res => res.json())
                .then(res => this.setState({ allPosts: res, noAction: false }))
                .catch(err => this.setState({noAction: false}));
        }

        getPostsByRelation() {
            this.setState({noAction: true})

            var arrayPost = []
            fetch(process.env.REACT_APP_USER_URL+`/friends/${this.props.keycloak.idTokenParsed.sub}`)
                .then(res => res.json())
                .then(res => {
                    res.push({idfriend: this.props.keycloak.idTokenParsed.sub})
                    res.map(
                        relation => {
                        fetch(process.env.REACT_APP_PUBLICATION_URL+`/users/${relation.idfriend}`, this.authorizationHeader())
                            .then(result => result.json())
                            .then(result => {
                                    
                                    result.map(post => {
                                        return arrayPost.push(post)
                                    })

                                    arrayPost
                                    .sort((a, b) => {
                                        const dateA = new Date(a.creation_date)
                                        const dateB = new Date(b.creation_date)

                                        let comparison = 0;
                                        if (dateA > dateB) {
                                            comparison = -1;
                                        } else if (dateA < dateB) {
                                            comparison = 1;
                                        }
                                        return comparison;
                                    })

                                    this.setState({ friendsPosts: arrayPost, onChange: false})
                                }
                            )
                            .catch(err => this.setState({noAction: false}));

                            return true;
                        }
                    )
                }
            )
            .catch(err => this.setState({noAction: false}));
        }

        
        componentDidMount() {
            this.getReaction()
            this.getPostsByRelation();
        }
        
        publish() {
            this.setState({noAction: true})

            fetch(process.env.REACT_APP_PUBLICATION_URL+"/posts", {
                method: 'POST',
                headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  "Authorization": "Bearer " + this.props.keycloak.token
                },
                body: JSON.stringify({
                    idUser: this.props.keycloak.idTokenParsed.sub,
                    message: this.state.publication_new_input
                })
            })  
                .then((res) => res.json())
                .then((res) => {
                    
                    const formData = new FormData()
                    formData.append('file', this.inputfile.current.files[0])
                    formData.append('idUser', this.props.keycloak.idTokenParsed.sub)
                    formData.append('idPost', res.id)

                    res.user = {
                        id: this.props.keycloak.idTokenParsed.sub,
                        first_name: this.props.keycloak.idTokenParsed.given_name,
                        last_name: this.props.keycloak.idTokenParsed.family_name,
                    }

                    res.message = this.state.publication_new_input
                    res.idUser = this.props.keycloak.idTokenParsed.sub
                    res.creation_date = new Date().toISOString()
                    console.log(res)
                    let friendsPosts = this.state.friendsPosts

                    friendsPosts.unshift(res)

                    fetch(process.env.REACT_APP_MEDIA_URL+"/files", {
                        method: 'POST',
                        body: formData
                    })
                        .then((e) => {
                            this.setState({friendsPosts: friendsPosts, publication_new_input: '', noAction: false})
                        })
                        .catch(err => {
                            this.inputfile.value = null
                            this.setState({noAction: false})
                        });   
                })
                .catch(err => this.setState({noAction: false}));
        }

        handleChange(event) {
            this.setState({[event.target.id]: event.target.value});
        }

        render() {
            console.log(this.state.noAction)
            if (this.state.friendsPosts.length === 0) {
                return (
                    <div className="publications-body">
                        <div className="publication">
                            <ion-card>
                            <div className="publication-header">                                
                                <div className="publication-header-name">Publier un budge</div>
                            </div>
                            <div className="publication-body">
                                <form>
                                    <textarea id="publication_new_input" placeholder="Cuicui" value={this.state.publication_new_input} maxLength="300" rows="1" onChange={this.handleChange}></textarea>
                                </form>
                            </div>
                            <div className="publication-footer">
                                <button id="publication-new-publier" disabled={!this.state.publication_new_input || this.state.noAction} onClick={this.publish}>Publier</button>
                                <div className="image-upload">
                                    <label htmlFor="file-input">
                                        <i className="fa fa-image"></i>
                                    </label>
                                    <input type="file" id="file-input" ref={this.inputfile} disabled={!this.state.noAction} /> 
                                </div>
                            </div>
                            </ion-card>
                        </div>
                        <div className="load-budges">Chargement <span role="img" aria-label="loading">🐦</span></div>
                    </div>
                );
            } else if (this.state.reactions!==[]){
                return ( 
                    <div className="publications-body">
                        <div className="publication">
                            <ion-card>
                            <div className="publication-header">                                
                                <div className="publication-header-name">Publier un budge</div>
                            </div>
                            <div className="publication-body">
                                <form>
                                    <textarea id="publication_new_input" placeholder="Cuicui" value={this.state.publication_new_input} maxLength="300" rows="1" onChange={this.handleChange}></textarea>
                                </form>
                            </div>
                            <div className="publication-footer">
                                <button id="publication-new-publier" disabled={!this.state.publication_new_input || this.state.noAction} onClick={this.publish}>Publier</button>
                                <div className="image-upload">
                                    <label htmlFor="file-input">
                                        <i className="fa fa-image"></i>
                                    </label>
                                    <input type="file" id="file-input" ref={this.inputfile} /> 
                                </div>
                            </div>
                            </ion-card>
                        </div>
                        {this.state.friendsPosts.map(item => (
                            <Publication item={item} key={item.id} keycloak={this.props.keycloak} onDelete={this.deletePost} reactions={this.state.reactions} />
                        ))}
                    </div>
                );
            }   
        }
}

export default Publications;