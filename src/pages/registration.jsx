import React, {Component} from 'react';
import Budgie_entier_white from '../img/Budgie_entier_white.png'
import 'primereact/resources/themes/nova-light/theme.css';
import 'primereact/resources/primereact.min.css';
import 'primeicons/primeicons.css';
import {Password} from 'primereact/password';
import {Button} from 'primereact/button';

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
    }   
  }

  render() {
    return (
      <div className="registration-page">
      <img className="titre-budgie" src={Budgie_entier_white} alt="titre Budgie"></img>
        <form className="registration-form" action="/auth">
          <div className="form-group">
              <label htmlFor="login">Identifiant</label>
              <input type="login" className="form-control" id="login" placeholder="Nom d'utilisateur"></input>
          </div>
          <div className="form-group">
              <label htmlFor="mail">Adresse mail</label>
              <input type="email" className="form-control" id="mail" placeholder="adresse@mail.com"></input>
          </div>
          <div className="form-group">
              <label htmlFor="password">Mot de passe</label>
              <Password value={this.state.value} onChange={(e) => this.setState({value: e.target.value})} weakLabel='Faible' mediumLabel='Moyen' strongLabel='Fort' promptLabel='Renseignez votre mot de passe'/>
          </div>
          <div className="form-group">
              <label htmlFor="bornDate">Date de naissance</label>
              <input type="date" className="form-control" id="bornDate"></input>
          </div>
          <Button type="submit" label="S'inscrire" className="p-button-secondary p-button-rounded register-button" formAction="/auth"/>
        </form>
      </div>
    );
  }
}
export default Registration;