import React from 'react';
import Profil from '../profile/profil'
import Header from '../components/header';

class ProfilPage extends React.Component {

    render() {
      return (
        <div className="home-page">
          <Header keycloak={this.props.keycloak}></Header>
          <Profil idUser={this.props.match.params.idUser} keycloak={this.props.keycloak}></Profil>
        </div>
      );
    }
  }

export default ProfilPage;
