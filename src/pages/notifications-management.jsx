import React from 'react';
import Notifications from '../notifications/notifications.jsx'

class NotificationsManagement extends React.Component {

    render() {
      return (
        <div className="notification-management-page">
          {/* <Header></Header> */}
          <Notifications></Notifications>
        </div>
      );
    }
  }

export default NotificationsManagement;
