import React from 'react';
import Publications from '../publication/publications.jsx'
import Header from '../components/header.jsx';

class Home extends React.Component {

    render() {
      return (
        <div className="home-page">
        <Header keycloak={this.props.keycloak}></Header>
        <Publications keycloak={this.props.keycloak}></Publications>
        </div>
      );
    }
  }

export default Home;
