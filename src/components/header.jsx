import React from 'react';
import Budgie_logo_white from '../img/logo_white.png';
import Logout from './logout';
import Autocomplete from 'react-autocomplete'
import { withRouter } from "react-router";

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      items: [],
      selected: null
    };

    this.onChange = this.onChange.bind(this)
  }

  onChange(value) {
    this.setState({value: value})

    if (value.length >= 3) {
      fetch(process.env.REACT_APP_USER_URL+`/users?filter=${value}`)    
        .then(res => res.json())
        .then(res => this.setState({ items: res }))
        .catch(err => console.log(err));
    } else {
      this.setState({items: []})
    }
  }

  onSelect(value) {
    this.setState({
      value: value,
      selected: this.state.items[0]
    })

    const idUser = this.state.items[0].id
    this.props.history.push(`/profils/${idUser}`)
  }

  render() {
    if (this.props.keycloak) {
      if (this.props.keycloak.authenticated) return (
        <div className="header">
          <a href="/home"><img src={Budgie_logo_white} alt="logo Budgie"/></a>

          <div className="title-header">
            <b>Budgie</b> Social Network
          </div>

          <div className="searchBar">
            <Autocomplete
              inputProps={{ placeholder: '   🔎Recherche Budgie' }}
              items={this.state.items}
              renderItem={(item, isHighlighted, i) =>
                <div style={{ background: isHighlighted ? 'lightgray' : 'white', color: 'black', height:'20px', width:'100%', overflow: 'hidden'} } key={i}>
                  <a href={'/profils/' + item.id} key={i} style={{height:'100%', position: 'relative', width:'100%', overflow: 'hidden', textDecoration: 'none'}}>{item.first_name + ' ' + item.last_name}</a>
                </div>
              }
              renderMenu={(items, value, style) => {
                return <div id="listItems" style={{ ...style, ...this.menuStyle }} children={items}/>
              }}
              value={this.state.value}
              onChange={(e) => this.onChange(e.target.value)}
              /*onSelect={(val) => this.onSelect(val)}*/
              getItemValue={(item) => `${item.first_name} ${item.last_name}`}
              menuStyle={{
                borderRadius: '3px',
                boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                background: 'rgba(255, 255, 255, 0.9)',
                padding: '2px 0',
                fontSize: '90%',
                position: 'fixed',
                maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom,
                overflow: 'hidden'
              }}
            />
          </div>
          <Logout keycloak={this.props.keycloak} />
          <div className="profile-name">
            <a href={"/profils/" + this.props.keycloak.idTokenParsed.sub}>
              <b>{ this.props.keycloak.idTokenParsed.family_name }</b> <span>{ this.props.keycloak.idTokenParsed.given_name }</span>
            </a>
          </div>
        </div>
      ); else return (<div>Unable to authenticate!</div>)
    }
    return (
      <div>Initializing Keycloak...</div>
    );
  }
}

export default withRouter(Header);
