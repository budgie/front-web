import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class Logout extends Component {
	logout() {
		this.props.history.push('/');
		this.props.keycloak.logout();
	}

	render() {
		return (
			<div className="logout-item">
				<i className="fa fa-sign-out fa-2x" aria-hidden="true" onClick={() => this.logout()} />
			</div>
		);
	}
}
export default withRouter(Logout);
