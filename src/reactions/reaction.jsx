import React from 'react';  
import './reaction.css'

class Reaction extends React.Component {
        constructor(props) {
            super(props)

            this.state = {
                idPost: this.props.idPost,
                idTypeReaction: this.props.idTypeReaction,
                nom: this.props.nom,
                valeur: this.props.valeur,
                countidreaction: 0,
                keycloak: null,
                authenticated: false
            }

            this.handleClick = this.handleClick.bind(this)
        }

        handleClick() {
            fetch(process.env.REACT_APP_REACTION_URL+`/reactions`, {
                method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        idPost: this.props.idPost, 
                        idUser: '2713aaaf-29fe-4568-8830-cf92a19f8da5', 
                        idTypeReaction: this.props.idTypeReaction
                    })
            })
                .then(res => res.json())
                .then(res => {
                    this.props.reloadFunction()
                })
                .catch(err => console.log(err));
        }

        componentDidMount() {
            console.log('etst')
            fetch(process.env.REACT_APP_REACTION_URL+`/post/${this.state.idPost}/type/${this.state.idTypeReaction}`)
                .then(res => res.json())
                .then(res => {console.log(res);this.setState({ countidreaction: res });this.render()})
                .catch(err => console.log(err));
        }

        render() {
            return (
                <div className="reaction" onClick={this.handleClick}>
                    <span role="img" aria-label={this.state.nom}>{this.state.valeur}</span>{this.state.countidreaction}
                </div>
            );
        }
}

export default Reaction;