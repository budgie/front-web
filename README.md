![Budgie logo](./src/img/Budgie_entier.png)

Budgie is a social network developed for a school project

## Installation

To launch the project, you need to go in the directory and first run:
### `npm install`

Then start it with :
### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

For it to work you have to have all the other project in this group running.

You can configure the links to those webservices by creating the file

### `./.env`

in the root of the project.
You will need to add the following code:

```
REACT_APP_AUTH_URL=<keycloak URL>
REACT_APP_MEDIA_URL=<media webservice URL>
REACT_APP_PUBLICATION_URL=<publication webservice URL>
REACT_APP_USER_URL=<user webservice URL>
REACT_APP_REACTION_URL=<reaction webservice URL>

```
